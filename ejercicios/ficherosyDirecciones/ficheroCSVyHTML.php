<?php
    $html="<html><body><table>";
    $archivoCSV = fopen("ficheroCSV.csv", "r");
    $celdaVacia="<td><input type=\"text\" value=\"\" /></td>";
    //analizo una línea desde el archivo abierto
    while (($line = fgetcsv($archivoCSV)) !== false) {
        //defino las filas
        $html=$html."<tr>";
        $numero = count($line);
        for ($c=0; $c < $numero; $c++) {
//Elimina espacios en blanco u otros caracteres al inicio y final de una cadena
            $datosCelda = trim($line[$c]);
            $columnas= ( $datosCelda===" " ) ? $celdaVacia :  "<td>$datosCelda</td>";
            $html=$html.$columnas;
        }
        $html= $html."</tr>";
    }
    fclose($archivoCSV);
    $html = $html."</table></body></html>";
    echo $html;
?>
