<?php
//Crea una clase que calcule el factorial de un número.
class Factorial {

    public function dameFactorial($numFactorial){
        $fact = 1;
        for($i= 1; $i<=$numFactorial; $i++) {
          $fact = $fact*$i;
        }
        return $fact;
    }
}
$num = 5;
$resultado = new Factorial();
echo "Factorial del número es ".$resultado->dameFactorial($num);
?>