<?php

/*Crea una clase que muestre un mensaje de bienvenida como 
“Hello All, I am Scott”, donde “Scott” 
es un argumento de un método llamado saluda
*/

class Saluda {
    public function metodo_saluda($nombre) {
        echo "Hola $nombre";
    }
}

$obj = new Saluda();
$obj->metodo_saluda("Iker");

?>