
<?php


/**Función que pasándole dos parámetros de entrada de tipo cadena escriba por la salida las dos cadenas unidas.
 * Ejemplo: "Pamela", "Gusqui"
 * "Hola Pamela Gusqui
*/


function hola_usuario($nombre,$apellido){
    echo "Hola $nombre $apellido";
}

$nom='Pamela';
$apelli='Gusqui';
hola_usuario($nom, $apelli);

?>

