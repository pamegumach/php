<?php
//Función que pasándole dos parámetros, altura y anchura. Calcular y retornar su área.

  function rectangulo($base, $altura){
    $area = $base*$altura; 
    return $area;
  }

  $base = 5;
  $altura = 3;
  $area= rectangulo($base,$altura);
  echo "El rectángulo con base: $base y altura: $altura <br>";
  echo "El área es: $area <br>";
?>
