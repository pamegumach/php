# El triangulo

Usando bucles for pintar por pantalla esta figura o similar, lo ideal que la altura pueda ser variable.

usar $numFilas como variable.


```
*
**
***
****
*****
```

Usaremos dos bucles For, el principal para recorrer las líneas de arriba abajo, el segundo bucle anidado recorrerá cada fila para imprimir los *, es decir, imprime tantos * como la línea en la que estemos, eso quiere decir que en el bucle anidado hacemos iteraciones desde 1 hasta el número de línea en la que estamos (contador bucle principal).

Es útil dibujar una tabla a mano para ver como se recorren los bucles

Para altura / nº filas = 5

| $i    |   $j  |
|-------|-------|
|  1    |   1   |
|  2    |   1   | 
|  2    |   2   | 
|  3    |   1   | 
|  3    |   2   | 
|  3    |   3   | 
..... (sigue)

