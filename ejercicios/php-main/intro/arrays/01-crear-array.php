<?php

//$miPrimerArray = array('Andres','Sergio','Iker');

$miPrimerArray = ['Andres','Sergio'];

// Lo mismo
$miPrimerArray[0] = 'Andres';
$miPrimerArray[1] = 'Sergio';
$miPrimerArray[2] = 'Iker';

// Lo mismo
//$miPrimerArray[] = 'Andres';

//siguiente
$miPrimerArray[] = 'Robson';

print_r($miPrimerArray);

$j = 0;
echo $miPrimerArray[$j++];
echo $miPrimerArray[$j++];

echo count($miPrimerArray);

if (isset($miPrimerArray[$j])) {
    echo "esta vacio!";
}

/////////////////////////////////////////////

$array = array(
    "foo" => "bar",
    "bar" => "foo",
    "bar" => "kaka"
);

echo $array["bar"];
echo count($array);

?>