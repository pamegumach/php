<?php
/*
$i = 11;
do {
    echo $i;
} while ($i > 0);
*/

//Romper la ejecución del bucle con break

$contador = 0;

while ($contador < 10) {
    if ($contador == 4) {
        echo "saliendo de bucle<br>";
        break;
    }
    $contador++;
}

echo "valor final de contador es:".$contador."<br>";

?>