<?php

$miPrimerArray = array('Pamela','Pepito','Juanita');

//Así accedo a la posición de mi array
$miPrimerArray[0] = 'Pamela';
$miPrimerArray[1] = 'Sergio';
$miPrimerArray[2]= 'Iker';

// Lo mismo
//$miPrimerArray[] = 'Chente';

//siguiente
$miPrimerArray[] = 'Chente';

print_r($miPrimerArray);

$j = 0;

echo $miPrimerArray[$j++];
echo $miPrimerArray[$j++];

//Tamaño de mi array
echo count($miPrimerArray);

if (isset($miPrimerArray[3])){
    echo "Está vacio, imbeshil!";
}


//Arrays asociativos
$array = array(
    "foo" => "bar",
    "bar" => "foo",
);

echo $array["bar"];
echo count($array);


?>