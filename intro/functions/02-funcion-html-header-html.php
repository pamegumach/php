<?php

/**
 * Función que imprime HTML cabecera de mi web
 */
function page_header($titulo,$color) {
    //Se usa el punto para concatenar
    //También se podría usar con echo y usar doble comillas.
    //También solo poner doble comillas
    /**
     * 	print "<html><head><title>$titulo</title></head>";
	 *  print "<body bgcolor=\"$color\">";
     */
	$cadena ="<html><head><title>$titulo</title></head>";
	$cadena.="<body bgcolor=\"$color\">";

    return $cadena;
}

/**
 * Función que imprime el body
 */
function page_body(){
    print '<h1>Mi primera web con funciones</h1>';
    print '<h3>Párrafo tomado de "El principito" </h3>';
    print '<p>Cuando el misterio es demasiado impresionante no es posible desobedecer.<br> 
    Es preciso que soporte dos o tres orugas si quiero conocer a las mariposas. <br>
    Hay que exigir a cada uno lo que cada uno puede hacer. <br> 
    Es mucho más difícil juzgarse a sí mismo que a los demás</p>';
}

/**
 * Función que imprime pie de página y cierra las etiquetas
 */

function page_footer(){
    return '</body></html>';
}


//-----------------------------------------------------------
//Aquí llamo a las funciones

$cadenaTitulo = "Primera página básica";
$color = "565B85";
$head = page_header($cadenaTitulo,$color);
//Aquí podemos crear el contenido de la web
page_body();
$footer = page_footer();
    echo $head;
    echo $footer;

?>