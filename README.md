<!--Esto es un título nivel 1 tipo h1-->
# README

<!--Para poner una línea horizontal-->
---

<!--Esto es un título nivel dos tipo h2-->
## Manuel MarkDown
Este es un manual sobre sintaxis MarkDown, al contrario que los editores como LibreOffice que son WYSIWYG


Esto es una **negrita**

Esto es _cursiva_.

**Listas**
* Elemento
* Elemento
* Elemento

Lista Ordenada
1. Uno 
2. Dos
3. Tres 

<!--METER CÓDIGO FUENTE DE ALGÚN CÓDIGO, EN MULTILINEA -->
```
<?php

echo "Hola Mundo";

?>
```

Código es una sola línea es así `echo "Hola Mundo!"`.

Un enlance se hace así [Texto del enlace](pamegumach.lovestoblog.com)

Incrustar una imagen:

![texto alt](img/gitImg.jpg)

> Esto es una cita de Autora: Pamela.


---

